extends Control

class Tile extends Node:
	var x: int
	var y: int
#	var color: Color # array of ints for byte ops
	var height_type: int
	var heat_value: float
	var precipitation: float
	var is_land: bool
	var collidable: bool
	var flood_filled: bool
	var left: Tile
	var right: Tile
	var top: Tile
	var bottom: Tile
	var bitmask: int
#	func _init():
	func check_neighbors(map, width):
		if x > 0:
			left = map[x - 1][y]
		else: left = null
		if x < width - 1:
			right = map[x + 1][y]
		else: right = null
		if y > 0:
			top = map[x][y - 1]
		else: top = null
		if y < width - 1:
			bottom = map[x][y + 1]
		else: bottom = null

	func update_bitmask(value: float = 0.15):
		var return_value = 0.00
		var count = 0
		if top is Tile:
			if top.height_type == height_type:
				count += 1
		elif top == null:
			count += 1
		if right is Tile:
			if right.height_type == height_type:
				count += 2
		elif right == null:
			count += 2
		if bottom is Tile:
			if bottom.height_type == height_type:
				count += 4
		elif bottom == null:
			count += 4
		if left is Tile:
			if left.height_type == height_type:
				count += 8
		elif left == null:
			count += 8
		bitmask = count
		if bitmask < 15:
			return_value -= value
		return return_value

var ELEVATION_TYPES = {
	DeepestWater = 0,
	DeepWater = 1,
	ShallowWater = 2,
	Sand = 3,
	Grass = 4,
	Forest = 5,
	Rock = 6,
	Snow = 7
}
var ELEVATION_VALUES: Dictionary = {
	DeepWater = 0.575,
	ShallowWater = 0.52,
	Sand = 0.5,
	Grass = 0.49,
	Forest = 0.45,
	Rock = 0.38,
	Snow = 0.31
}
var EQUATORS = {
	Vertical = 0,
	Horizontal = 1,
}
var HEIGHT_COLORS: Dictionary = {
	DeepestWater = rgb(33, 70, 122),
	DeepWater = rgb(63, 100, 152),
	ShallowWater = rgb(176, 230, 244),
	Sand = rgb(240, 240, 64),
	Grass = rgb(50, 220, 20),
	Forest = rgb(16, 160, 0),
	Rock = rgb(127, 127, 127),
	Snow = rgb(255, 255, 255)
}
var HEAT_COLORS: Dictionary = {
	Low = rgb(130, 247, 255, 130),
	High = rgb(255, 65, 65, 130)
}
var PRECIP_COLORS: Dictionary = {
	Low = rgb(103, 0, 31),
	High = rgb(5, 48, 97)
}


var map: Array = []

export var width: int = 512
export var height: int = width
var tiles_total = float(width) * float(height)
# warning-ignore:integer_division
var max_size = 512.0
var world_scale = Vector2(max_size / float(width),max_size / float(width))

#var thread

var simplex_noise = OpenSimplexNoise.new() as OpenSimplexNoise

var humid_alpha: int = 100
var noise_period: float = 32.0
var noise_persistence: float = 0.37
var noise_lacunarity: float = 2.04
var noise_octaves: int = 6
var world_equator: int = EQUATORS.Horizontal

var image_samples_path = "res://image_samples/"
var gradient_path = "data/gradient/"
var gradient_width = 1024.0

func _ready():
# warning-ignore:return_value_discarded
	set_process(true)

#	start_generation()
	pass

func _process(delta):
	if Input.is_action_just_released("quit"):
		get_tree().quit()
		pass

func start_generation(world_size: Vector2 = Vector2(width, height)):
	var thread = Thread.new()
	if thread.is_active():
		call_deferred("start_generation")
		return
	else:
		width = int(world_size.x)
		height = width
		thread.start(self, "generate_map", thread)
	pass


func noise_to_type(pixel: float, color_type: String):
	if pixel < ELEVATION_VALUES.Snow:
		return "Snow"
	if pixel < ELEVATION_VALUES.Rock:
		return "Rock"
	if pixel < ELEVATION_VALUES.Forest:
		return "Forest"
	if pixel < ELEVATION_VALUES.Grass:
		return "Grass"
	if pixel < ELEVATION_VALUES.Sand:
		return "Sand"
	if pixel < ELEVATION_VALUES.ShallowWater:
		return "ShallowWater"
	if pixel < ELEVATION_VALUES.DeepWater:
		return "DeepWater"
	else:
		return "DeepestWater"
	pass

func confirm_internal_data():
	tiles_total = float(width) * float(height)
	world_scale = Vector2(max_size / float(width),max_size / float(width))
	pass

func generate_map(_thread : Thread, equator = world_equator, octaves: int = noise_octaves, period: float = noise_period,
	persistence: float = noise_persistence, lacunarity: float = noise_lacunarity):
	#start
	confirm_internal_data()


	var equator_path
	match equator:
		EQUATORS.Horizontal:
			equator_path = "horizontal/"
		EQUATORS.Vertical:
			equator_path = "vertical/"

	var file = File.new()
	var gradient_files = list_files_in_directory(image_samples_path + gradient_path + equator_path)
	var path = image_samples_path + gradient_path + equator_path + gradient_files[randi() % gradient_files.size()]
	# load gradient array file
	if file.open(path, File.READ) != OK:
		push_error("Failed to open file: " + path)
	var gradient = file.get_var() as Array
	file.close()
	var gradient_scale = float(gradient_width) / float(width)
	if gradient_scale > 1:
		gradient = shrink_2d_array(gradient, gradient_scale)
	
	if map.size() > 0:
		clear_map(map)
	
	map = create_2d_array(width, height)
	simplex_noise.octaves = octaves
	simplex_noise.period = period
	if int(world_scale.x) != 1:
		simplex_noise.period = period / (world_scale.x * 0.25)
	simplex_noise.persistence = persistence
	simplex_noise.lacunarity = lacunarity
	simplex_noise.seed = randi()
	var height_map = simplex_noise.get_seamless_image(width)
	simplex_noise.seed = randi()
	var precip_map = simplex_noise.get_seamless_image(width)
	height_map.lock()
	precip_map.lock()
	
	#Generate terrain from OpenSimplex, then sort the tiles into 2 groups, Land and Water
	for x in range(width):
		for y in range(height):
			var noise_sample = height_map.get_pixel(x, y).v


			# Height
			var type = noise_to_type(noise_sample, "main")
			map[x][y].height_type = ELEVATION_TYPES[type]
			if HEIGHT_COLORS[type] == HEIGHT_COLORS.DeepestWater || HEIGHT_COLORS[type] == HEIGHT_COLORS.DeepWater || HEIGHT_COLORS[type] == HEIGHT_COLORS.ShallowWater:
				map[x][y].is_land = true
			
			#Heat
			var heat_sample = clamp((gradient[x][y] * 0.75) / noise_sample, 0.00, 1.00)
			map[x][y].heat_value = heat_sample
			
			#Precipitation
			map[x][y].precipitation = clamp(precip_map.get_pixel(x, y).v * 0.65, 0.00, 1.00)
			
			#end
	#We then check all tiles' neighbors
	for x in range(width):
		for y in range(height):
			map[x][y].check_neighbors(map, width)
			var bitmask_value = map[x][y].update_bitmask()
	#end
	_thread.wait_to_finish()
	pass



func create_2d_array(width, height):
	var a = []

	for y in range(height):
		a.append([])
		a[y].resize(width)

		for x in range(width):
			a[y][x] = Tile.new()

	return a

func heat_to_color(value):
	return HEAT_COLORS.High.linear_interpolate(HEAT_COLORS.Low, value)
	pass

func precip_to_color(value):
	return PRECIP_COLORS.High.linear_interpolate(PRECIP_COLORS.Low, value)
	pass

func create_voronoi_diagram(imgSize : Vector2, num_cells: int):

	var img = Image.new()
	img.create(imgSize.x, imgSize.y, false, Image.FORMAT_RGBH)
	img.lock()

	var points = []
	var colors = []

	for i in range(num_cells):
		points.push_back(Vector2(int(randf()*img.get_size().x), int(randf()*img.get_size().y)))

		randomize()
		var colorPossibilities = [ Color.blue, Color.red, Color.green, Color.purple, Color.yellow, Color.orange]
		colors.push_back(colorPossibilities[randi()%colorPossibilities.size()])

	for y in range(img.get_size().y):
		for x in range(img.get_size().x):
			var dmin = img.get_size().length()
			var j = -1
			for i in range(num_cells):
				var d = (points[i] - Vector2(x, y)).length()
				if d < dmin:
					dmin = d
					j = i
			img.set_pixel(x, y, colors[j])

	img.unlock()
	var texture = ImageTexture.new()
	texture.create_from_image(img)
	return texture

func clear_map(map):
	for x in map.size():
		for y in map[x].size():
#			if is_instance_valid(map[x][y]):
			map[x][y].free()
#			else:
#				map[x][y] = null
			pass
	map.clear()
	pass

func shrink_2d_array(array, scale):
	var a = []
	for x in range(array.size() / scale):
		a.append([])
		a[x].resize(array.size() / scale)

		for y in range(array.size() / scale):
			a[x][y] = 0.0

	var xx = 0
	var yy = 0
	for x in (a.size()):
		for y in (a.size()):
			a[x][y] = array[xx][yy]

			if y == 0:
				yy = 0
			else:
				yy += scale
			pass
		#end x
		if x == 0:
			xx = 0
		else:
			xx += scale
	return a
	pass

func expand_2d_array(array, scale):
	var a = []
	var x = 0
	var y = 0
	while x < array.size() - 1:
		a.append([])
		while y < x.size() - 1:
			a[x][y] = array[x][y]

			#end y
			y += scale
			pass
		#end x
		x += scale
	return a
	pass

func rgb(r: int, g: int, b: int, a: int = 255):
	var color = Color(0.0,0.0,1.0)
	color.r8 = r
	color.g8  = g
	color.b8 = b
	color.a8 = a
	return color
	pass

func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()

	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with(".") && not file.ends_with(".import"):
			files.append(file)

	dir.list_dir_end()

	return files

